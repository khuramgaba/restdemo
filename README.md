
# Use the following maven command to build the executable jar:

mvn clean package




# After build has finished, you can run the executable jar by running following command:

java -jar target/RestDemo-0.0.1-SNAPSHOT.jar



# Here are the REST endpoints for the People and the Family APIs:

http://localhost:8080/rest/people

http://localhost:8080/rest/family


# UI link

http://localhost:8080/index.html

