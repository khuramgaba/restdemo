package com.restdemo.controller;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restdemo.data.Family;
import com.restdemo.data.Person;
import com.restdemo.repository.FamilyCrudRepository;
import com.restdemo.repository.PersonCrudRepository;


@RestController
@RequestMapping("/rest/families")
public class FamilyController {

	@Autowired
	private FamilyCrudRepository familyCrudRepo;

	@Autowired
	private PersonCrudRepository personCrudRepo;

	@RequestMapping(method= RequestMethod.POST)
	public Family create(@RequestBody Family family){

		if(family.getMembers() != null && family.getMembers().size() > 0){
			Set <Person> people = new HashSet<>();
			for(Person person : family.getMembers()){
				if(person.getId() != null){
					people.add(personCrudRepo.findOne(person.getId()));
				}
			}
			family.setMembers(people);
		}

		familyCrudRepo.save(family);
		return family;
	}
	
	@RequestMapping(method= RequestMethod.GET)
	public Collection<Family> getAll(){
		
		return (Collection<Family>) familyCrudRepo.findAll();
	}
	
	@RequestMapping(value = "{id}", method= RequestMethod.GET)
	public Family get(@PathVariable("id") Integer id){
		
		return familyCrudRepo.findOne(id);
	}
	
	
	@RequestMapping(value = "{id}", method= RequestMethod.DELETE)
	public void delete(@PathVariable("id") Integer id){
		
		familyCrudRepo.delete(id);
	}
	
	

}
