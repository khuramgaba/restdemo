package com.restdemo.controller;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.restdemo.data.Person;
import com.restdemo.repository.PersonCrudRepository;


@RestController
@RequestMapping("/rest/people")
public class PersonController {
	
	@Autowired
	private PersonCrudRepository peopleRepo;

	
	@RequestMapping(method= RequestMethod.GET)
	public Collection<Person> getAll(){
		
		return (Collection<Person>) peopleRepo.findAll();
	}
	
	@RequestMapping(method= RequestMethod.POST)
	public Person create(@RequestBody Person people){
		
		peopleRepo.save(people);
		return people;
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	public void delete(@PathVariable("id") Integer id){
		
		peopleRepo.delete(id);
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	public Person update(@PathVariable("id") Integer id, @RequestBody Person people){
		
		Person entity = peopleRepo.findOne(id);
		people.setId(entity.getId());
		
		peopleRepo.save(people);
		
		return people;
		
	}
	
	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public Person get(@PathVariable("id") Integer id){
		
		return peopleRepo.findOne(id);
		
	}
	
	

}
