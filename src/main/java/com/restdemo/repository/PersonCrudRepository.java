package com.restdemo.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.restdemo.data.Person;

@Repository
public interface PersonCrudRepository extends CrudRepository <Person, Integer>{

}
