package com.restdemo.repository;

import org.springframework.data.repository.CrudRepository;

import com.restdemo.data.Family;

public interface FamilyCrudRepository extends CrudRepository<Family, Integer>{

}
