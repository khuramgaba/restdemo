
angular.module('restDemo',['ui.router','ngResource','restDemo.controllers','restDemo.services']);

angular.module('restDemo').config(function($stateProvider,$httpProvider){
    $stateProvider.state('people',{
        url:'/people',
        templateUrl:'templates/people.html',
        controller:'PeopleController'
    }).state('viewPeople',{
       url:'/people/:id/view',
       templateUrl:'templates/people-view.html',
       controller:'PeopleViewController'
    }).state('newPeople',{
        url:'/people/new',
        templateUrl:'templates/people-add.html',
        controller:'PeopleCreateController'
    }).state('editPeople',{
        url:'/people/:id/edit',
        templateUrl:'templates/people-edit.html',
        controller:'PeopleEditController'
    });
}).run(function($state){
   $state.go('people');
});