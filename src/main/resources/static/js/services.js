
angular.module('restDemo.services',[]).factory('People',function($resource){
    return $resource('http://localhost:8080/rest/people/:id',{id:'@id'},{
        update: {
            method: 'PUT'
        }
    });
}).service('popupService',function($window){
    this.showPopup=function(message){
        return $window.confirm(message);
    }
});