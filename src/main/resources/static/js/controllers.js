
angular.module('restDemo.controllers',[]).controller('PeopleController',function($scope,$state,popupService,$window,People){

    $scope.people=People.query();

    $scope.deletePeople=function(people){
        if(popupService.showPopup('Do you want to delete this person?')){
            people.$delete(function(){
                $window.location.href='';
            });
        }
    }

}).controller('PeopleViewController',function($scope,$stateParams,People){

    $scope.people=People.get({id:$stateParams.id});

}).controller('PeopleCreateController',function($scope,$state,$stateParams,People){

    $scope.people=new People();

    $scope.addPeople=function(){
        $scope.people.$save(function(){
            $state.go('people');
        });
    }

}).controller('PeopleEditController',function($scope,$state,$stateParams,People){

    $scope.updatePeople=function(){
        $scope.people.$update(function(){
            $state.go('people');
        });
    };

    $scope.loadpeople=function(){
        $scope.people=People.get({id:$stateParams.id});
    };

    $scope.loadpeople();
});