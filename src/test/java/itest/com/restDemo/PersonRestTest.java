package itest.com.restDemo;

import java.util.List;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.restdemo.Application;
import com.restdemo.data.Person;
import com.restdemo.repository.PersonCrudRepository;

@SpringApplicationConfiguration(classes=Application.class)
@WebIntegrationTest
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonRestTest {


	private RestTemplate restTemplate = new TestRestTemplate();

	public static final Gson gsonParser = new Gson();

	@Autowired
	private PersonCrudRepository personRepo;



	@Test
	public void testCreate(){

		Person payload = new Person();

		payload.setFirstName("Khuram");
		payload.setLastName("Gaba");
		payload.setPhoneNumber("630-618-1760");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> httpEntity = new HttpEntity<>(gsonParser.toJson(payload), headers);

		Person person = restTemplate.postForObject("http://localhost:8080/rest/people", httpEntity, Person.class);

		int id = person.getId();

		assertNotNull(id);

		person = restTemplate.getForObject("http://localhost:8080/rest/people/"+id, Person.class);

		assertEquals("Khuram", person.getFirstName());
		assertEquals("Gaba", person.getLastName());
		
		personRepo.delete(id);


	}
	
	@Test
	public void testDelete(){

		Person payload = new Person();

		payload.setFirstName("Khuram");
		payload.setLastName("Gaba");
		payload.setPhoneNumber("630-618-1760");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> httpEntity = new HttpEntity<>(gsonParser.toJson(payload), headers);

		Person person = restTemplate.postForObject("http://localhost:8080/rest/people", httpEntity, Person.class);

		int id = person.getId();

		assertNotNull(id);

		person = restTemplate.getForObject("http://localhost:8080/rest/people/"+id, Person.class);

		assertEquals("Khuram", person.getFirstName());
		assertEquals("Gaba", person.getLastName());
		
		restTemplate.delete("http://localhost:8080/rest/people/"+id);
		
		person = restTemplate.getForObject("http://localhost:8080/rest/people/"+id, Person.class);
		
		assertNull(person);


	}
	
	@Test
	public void testUpdate(){

		Person payload = new Person();

		payload.setFirstName("Khuram");
		payload.setLastName("Gaba");
		payload.setPhoneNumber("630-618-1760");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> httpEntity = new HttpEntity<>(gsonParser.toJson(payload), headers);

		Person person = restTemplate.postForObject("http://localhost:8080/rest/people", httpEntity, Person.class);

		int id = person.getId();

		assertNotNull(id);

		person = restTemplate.getForObject("http://localhost:8080/rest/people/"+id, Person.class);

		assertEquals("Khuram", person.getFirstName());
		assertEquals("Gaba", person.getLastName());
		
		
		payload.setFirstName("Safa");
		httpEntity = new HttpEntity<>(gsonParser.toJson(payload), headers);
		
		person = restTemplate.exchange("http://localhost:8080/rest/people/"+id,HttpMethod.PUT, httpEntity, Person.class).getBody();
		
		person = restTemplate.getForObject("http://localhost:8080/rest/people/"+id, Person.class);
		
		assertEquals("Safa", person.getFirstName());
		
		personRepo.delete(id);


	}
	
	@Test
	public void testGetAll(){

		Person payload1 = new Person();

		payload1.setFirstName("Khuram");
		payload1.setLastName("Gaba");
		payload1.setPhoneNumber("630-618-1760");
		
		Person payload2 = new Person();

		payload2.setFirstName("Safa");
		payload2.setLastName("Gaba");
		payload2.setPhoneNumber("630-618-1760");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> httpEntity = new HttpEntity<>(gsonParser.toJson(payload1), headers);

		Person person = restTemplate.postForObject("http://localhost:8080/rest/people", httpEntity, Person.class);

		int person1Id = person.getId();

		assertNotNull(person1Id);
		
		
		httpEntity = new HttpEntity<>(gsonParser.toJson(payload2), headers);

		person = restTemplate.postForObject("http://localhost:8080/rest/people", httpEntity, Person.class);

		int person2Id = person.getId();

		assertNotNull(person2Id);

		List<Person> people = restTemplate.getForObject("http://localhost:8080/rest/people/", List.class);
		
		assertEquals(2, people.size());
		
		personRepo.delete(person1Id);
		personRepo.delete(person2Id);


	}
}
