package itest.com.restDemo;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.util.HashSet;
import java.util.Set;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.boot.test.TestRestTemplate;
import org.springframework.boot.test.WebIntegrationTest;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.RestTemplate;

import com.google.gson.Gson;
import com.restdemo.Application;
import com.restdemo.data.Family;
import com.restdemo.data.Person;
import com.restdemo.repository.FamilyCrudRepository;
import com.restdemo.repository.PersonCrudRepository;

@SpringApplicationConfiguration(classes=Application.class)
@WebIntegrationTest
@RunWith(SpringJUnit4ClassRunner.class)
public class FamilyRestTest {

	
	private RestTemplate restTemplate = new TestRestTemplate();

	public static final Gson gsonParser = new Gson();

	@Autowired
	private PersonCrudRepository personRepo;
	
	@Autowired
	private FamilyCrudRepository familyRepo;
	
	
	
	@Test
	public void testCreate(){

		Person personPayload1 = new Person();

		personPayload1.setFirstName("Khuram");
		personPayload1.setLastName("Gaba");
		personPayload1.setPhoneNumber("630-618-1760");
		
		Person personPayload2 = new Person();

		personPayload2.setFirstName("Safa");
		personPayload2.setLastName("Gaba");
		personPayload2.setPhoneNumber("630-618-1760");

		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> httpEntity = new HttpEntity<>(gsonParser.toJson(personPayload1), headers);

		Person person = restTemplate.postForObject("http://localhost:8080/rest/people", httpEntity, Person.class);

		int person1Id = person.getId();

		assertNotNull(person1Id);
		
		
		httpEntity = new HttpEntity<>(gsonParser.toJson(personPayload2), headers);

		person = restTemplate.postForObject("http://localhost:8080/rest/people", httpEntity, Person.class);

		int person2Id = person.getId();

		assertNotNull(person2Id);
		
		Family familyPayload = new Family();
		familyPayload.setName("Gaba Family");
		
		Set<Person> members = new HashSet<>();
		
		Person member1 = new Person();
		member1.setId(person1Id);
		
		Person member2 = new Person();
		member2.setId(person2Id);
		
		members.add(member1);
		members.add(member2);
		
		familyPayload.setMembers(members);
		
		httpEntity = new HttpEntity<>(gsonParser.toJson(familyPayload), headers);
		
		Family family = restTemplate.postForObject("http://localhost:8080/rest/families", httpEntity, Family.class);
		
		int familyId = family.getId();

		family = restTemplate.getForObject("http://localhost:8080/rest/families/"+familyId, Family.class);
		
		assertEquals("Gaba Family", family.getName());
		assertEquals(2, family.getMembers().size());
		
		familyRepo.delete(familyId);
		personRepo.delete(person1Id);
		personRepo.delete(person2Id);


	}
	
	
}
